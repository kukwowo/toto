# -*- coding: utf-8 -*-
# Copyright (C) 2014-2017 Andrey Antukh <niwi@niwi.nz>
# Copyright (C) 2014-2017 Jes�s Espino <jespinog@gmail.com>
# Copyright (C) 2014-2017 David Barrag�n <bameda@dbarragan.com>
# Copyright (C) 2014-2017 Alejandro Alonso <alejandro.alonso@kaleidos.net>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#from .development import *
from .common import *
#########################################
## GENERIC
#########################################

#DEBUG = False

ADMINS = (
    ("Admin", "example@example.com"),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('TAIGA_DB_NAME'),
        'HOST': os.getenv('TAIGA_DB_HOST'),
        'USER': os.getenv('TAIGA_DB_USER'),
        'PASSWORD': os.getenv('TAIGA_DB_PASSWORD')
    }
}


MEDIA_URL = "http://taiga-back:8000/media/"
STATIC_URL = "http://taiga-back:8000/static/"
ADMIN_MEDIA_PREFIX = "http://taiga-back:8000/static/admin/"
SITES["front"]["scheme"] = "http"
SITES["front"]["domain"] = "taiga-back:8000"
SECRET_KEY = "taiga"

DEBUG = True
PUBLIC_REGISTER_ENABLED = False

#DEFAULT_FROM_EMAIL = "no-reply@example.com"
#SERVER_EMAIL = DEFAULT_FROM_EMAIL
#CELERY_ENABLED = True

EVENTS_PUSH_BACKEND = "taiga.events.backends.rabbitmq.EventsPushBackend"
EVENTS_PUSH_BACKEND_OPTIONS = {"url": "amqp://taiga:taiga@taiga-rabbit:5672/taiga"}